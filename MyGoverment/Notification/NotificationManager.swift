import Foundation
import UserNotifications

class Notifications: NSObject, UNUserNotificationCenterDelegate {
    
    static let shared = Notifications()
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    func userRequest() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    
    private func getTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm, MMM d"
        formatter.locale = Locale(identifier: "ru_RU")
        return formatter.string(from: date)
    }
    
    func scheduleMorningNotification() {
        let identifier = "morning"
        let dateComponents = DateComponents(calendar: Calendar.current,
                                            timeZone: TimeZone.current,
                                            hour: 8,
                                            minute: 5)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        scheduleNotification(identifier: identifier,
                             title: nil,
                             body: "Hmmm, I guess, it’s great time to get down to business. You can learn something new.",
                             trigger: trigger)
        
        let bonusAction = UNNotificationAction(identifier: "Bonus", title: "Get $1.5K Bonus", options: [.foreground])
        let notYetAction = UNNotificationAction(identifier: "NotYet", title: "Not yet", options: [.destructive])
        
        let category = UNNotificationCategory(identifier: "\(identifier)-user-actions",
                                              actions: [bonusAction, notYetAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
    }
    
    func scheduleOnFinishLineNotification() {
        guard let elapsedSeconds = UserDefaults.standard.value(forKey: "kTimerTime") as? Double,
            let localTime = UserDefaults.standard.value(forKey: "kTime") as? Date else { return }
        
        let leftTime = Int((maxSeconds-maxSeconds/3) - elapsedSeconds)
        guard let futureDate = Calendar.current.date(byAdding: .second,
                                                     value: leftTime,
                                                     to: localTime) else { return }
        let identifier = "finish-line"
        let calendar = Calendar.current
        let dateComponents = DateComponents(calendar: calendar,
                                            timeZone: TimeZone.current,
                                            hour: calendar.component(.hour, from: futureDate),
                                            minute: calendar.component(.minute, from: futureDate),
                                            second: calendar.component(.second, from: futureDate))
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        scheduleNotification(identifier: identifier,
                             title: nil,
                             body: "Come on, you’re almost reached it!",
                             trigger: trigger)
    }
    
    func scheduleFinishNotification() {
        guard let elapsedSeconds = UserDefaults.standard.value(forKey: "kTimerTime") as? Double,
            let localTime = UserDefaults.standard.value(forKey: "kTime") as? Date else { return }
        
        let leftTime = Int(maxSeconds - elapsedSeconds)
        guard let futureDate = Calendar.current.date(byAdding: .second,
                                                     value: leftTime,
                                                     to: localTime) else { return }
        let identifier = "finish"
        let calendar = Calendar.current
        let dateComponents = DateComponents(calendar: calendar,
                                            timeZone: TimeZone.current,
                                            hour: calendar.component(.hour, from: futureDate),
                                            minute: calendar.component(.minute, from: futureDate),
                                            second: calendar.component(.second, from: futureDate))
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        scheduleNotification(identifier: identifier,
                             title: nil,
                             body: "Congrats! Now you choose your own new business for 1.5 hour",
                             trigger: trigger)
    }
    
    func scheduleWorkingNotification() {
        for index in 2...4 {
            guard let elapsedSeconds = UserDefaults.standard.value(forKey: "kTimerTime") as? Double,
                let localTime = UserDefaults.standard.value(forKey: "kTime") as? Date else { return }
            
            let leftTime = Int(maxSeconds - elapsedSeconds)/index
            guard let futureDate = Calendar.current.date(byAdding: .second,
                                                         value: leftTime,
                                                         to: localTime) else { return }
            let identifier = "working-\(index)"
            let calendar = Calendar.current
            let dateComponents = DateComponents(calendar: calendar,
                                                timeZone: TimeZone.current,
                                                hour: calendar.component(.hour, from: futureDate),
                                                minute: calendar.component(.minute, from: futureDate),
                                                second: calendar.component(.second, from: futureDate))
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
            
            scheduleNotification(identifier: identifier,
                                 title: nil,
                                 body: "All right, dude, you’re going cool. Now don’t distracted by this app! Cope with your business.",
                                 trigger: trigger)
        }
    }
    
    func schedulePerDayNotification() {
        for index in 8...22 {
            guard let isWorking = UserDefaults.standard.value(forKey: "kIsWorking") as? Bool else { return }
            
            if isWorking == true { return }
            let identifier = "per-day-\(index)"
            let dateComponents = DateComponents(calendar: Calendar.current,
                                                timeZone: TimeZone.current,
                                                hour: index,
                                                minute: 17)
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            
            scheduleNotification(identifier: identifier,
                                 title: nil,
                                 body: "There are \(24-index) hours left until the end of the day. Hurry up.",
                                 trigger: trigger)
        }
    }
    
    func removePendingNotificationRequests() {
        notificationCenter.removeAllPendingNotificationRequests()
    }
    
    private func scheduleNotification(identifier: String, title: String?, body: String, trigger: UNCalendarNotificationTrigger) {
        let userActions = "\(identifier)-user-actions"
        
        let content = UNMutableNotificationContent()
        content.title = title ?? ""
        content.body = body
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = userActions
        
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content,
                                            trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Bonus":
            print("Snooze")
            addMoney()
        case "NotYet":
            print("Delete")
        default:
            print("Unknown action")
        }
        completionHandler()
    }
    
    private func addMoney() {
        UserDefaults.standard.set(true, forKey: "kIsBonus")
    }
}
