import Foundation

extension UserDefaults {
    class func SFSDefault(setIntegerValue integer: Int, forKey key: String) {
        UserDefaults.standard.set(integer, forKey: key)
        UserDefaults.standard.synchronize()
    }

    class func SFSDefault(setObject object: Any, forKey key: String) {
        UserDefaults.standard.set(object, forKey: key)
        UserDefaults.standard.synchronize()
    }

    class func SFSDefault(setValue object: Any, forKey key: String) {
        UserDefaults.standard.setValue(object, forKey: key)
        UserDefaults.standard.synchronize()
    }

    class func SFSDefault(setBool boolObject: Bool, forKey key: String) {
        UserDefaults.standard.set(boolObject, forKey: key)
        UserDefaults.standard.synchronize()
    }

    class func SFSDefault(integerForKey  key: String) -> Int {
        let integerValue: Int = UserDefaults.standard.integer(forKey: key) as Int
        UserDefaults.standard.synchronize()
        return integerValue
    }

    class func SFSDefault(objectForKey key: String) -> Any {
        let object: Any = UserDefaults.standard.object(forKey: key) as Any
        UserDefaults.standard.synchronize()
        return object
    }

    class func SFSDefault(valueForKey  key: String) -> Any {
        let value: Any = UserDefaults.standard.value(forKey: key) as? Any ?? ""
        UserDefaults.standard.synchronize()
        return value
    }

    class func SFSDefault(boolForKey  key: String) -> Bool {
        let booleanValue: Bool = UserDefaults.standard.bool(forKey: key) as Bool
        UserDefaults.standard.synchronize()
        return booleanValue
    }

    class func SFSDefault(removeObjectForKey key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }

    //Save no-premitive data
    class func SFSDefault(setArchivedDataObject object: Any, forKey key: String) {
        if let data  = NSKeyedArchiver.archivedData(withRootObject: object) as? Data {
            UserDefaults.standard.set(data, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }

    class func SFSDefault(getUnArchiveObjectforKey key: String) -> Any? {
        var objectValue: Any?
        if  let storedData  = UserDefaults.standard.object(forKey: key) as? Data {
            objectValue   =  NSKeyedUnarchiver.unarchiveObject(with: storedData) as Any?
            UserDefaults.standard.synchronize()
            return objectValue!
        } else {
            objectValue = "" as Any?
            return objectValue!
        }
    }
}
