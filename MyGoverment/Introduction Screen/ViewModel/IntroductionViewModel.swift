import Foundation

protocol IntroductionView {
    func showMainScreen()
}

class IntroductionViewModel: ViewModel {
    
    let view: IntroductionView
    
    
    init(view: IntroductionView) {
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    public func letsGoButtonPressed() {
        UserDefaults.standard.set(false, forKey: "kIsFirstStart")
        view.showMainScreen()
    }
    
}
