import UIKit

class IntroductionViewController: BaseViewController<IntroductionViewModel>, IntroductionView {
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var letsGoButton: UIButton! {
        didSet {
            letsGoButton.cornerRadius(radius: 18.0)
        }
    }
    @IBOutlet weak var oneScreenWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bicycleCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var letsGoButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var letsGoButtonLeadingConstraint: NSLayoutConstraint!
    
    var isBicycleAnimated: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl.numberOfPages = 5
        pageControl.currentPage = 0
        
        let frame = view.frame
        oneScreenWidthConstraint.constant =  frame.width
        letsGoButtonWidthConstraint.constant = 0
        letsGoButtonLeadingConstraint.constant = (frame.width-290.0)/2
        
        self.view.layoutIfNeeded()
    }
    
    private func animateBicycle() {
        if isBicycleAnimated == true { return }
        isBicycleAnimated = true
        bicycleCenterXConstraint.constant = view.frame.width
        letsGoButtonWidthConstraint.constant = 290.0
        UIView.animate(withDuration: 1.75, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func showMainScreen() {
        Coordinator.shared.showMainScreen()
    }
    
    @IBAction func letsGoButtonPressed(_ sender: Any) {
        viewModel.letsGoButtonPressed()
    }
    
}

extension IntroductionViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        // Bicycle Animation
        if pageControl.currentPage == 4 {
            animateBicycle()
        }
    }
}
