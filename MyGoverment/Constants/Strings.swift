// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum Main {
    internal enum Screen {
      internal enum Label {
        /// 2 Handshakes to your master
        internal static let main = L10n.tr("Localizable", "main.screen.label.main")
        /// Popular searches
        internal static let popularsearch = L10n.tr("Localizable", "main.screen.label.popularsearch")
      }
      internal enum Textfield {
        /// New York...
        internal static let locationsearch = L10n.tr("Localizable", "main.screen.textfield.locationsearch")
        /// Spanish teacher...
        internal static let search = L10n.tr("Localizable", "main.screen.textfield.search")
      }
    }
  }

  internal enum Search {
    internal enum Screen {
      internal enum Label {
        /// results for
        internal static let resultsfor = L10n.tr("Localizable", "search.screen.label.resultsfor")
      }
      internal enum Textfield {
        /// Spanish teacher...
        internal static let search = L10n.tr("Localizable", "search.screen.textfield.search")
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
