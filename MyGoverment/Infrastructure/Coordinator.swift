import UIKit

class Coordinator {

    static let shared = Coordinator()

    private var compositionRoot: CompositionRoot {
        return CompositionRoot.sharedInstance
    }
    
    private var tabBarController: UITabBarController {
        return compositionRoot.rootTabBarController
    }

    private var baseNavigationController: UINavigationController? {
        return lastPresentedViewController?.navigationController
    }

    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var sceneDelegate: SceneDelegate {
        return UIApplication.shared.delegate as! SceneDelegate
    }

    private var lastPresentedViewController: UIViewController?

    private init() {}

    func showRootTabBarController() {
        let tabBarController = compositionRoot.rootTabBarController
        let navController = UINavigationController(rootViewController: compositionRoot.rootTabBarController)
        navController.navigationBar.isHidden = true
        tabBarController?.view.window?.rootViewController = navController
    }
    
    func showSuccessScreen() {
        present(compositionRoot.resolveSuccessViewController())
    }
    
    func showIntroScreen() {
        guard let window = selfWindow else { return }
        let vc = compositionRoot.resolveIntroductionViewController()
        window.rootViewController = vc
        UIView.transition(with: window,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {},
                          completion: nil)
    }
    
    func showMainScreen() {
        guard let window = selfWindow else { return }
        let vc = compositionRoot.resolveMainViewController()
        window.rootViewController = vc
        lastPresentedViewController = vc
        UIView.transition(with: window,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {},
                          completion: nil)
    }

    func push(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.pushViewController(vc, animated: true)
            lastPresentedViewController = vc
        }
    }
    
    func present(_ vc: UIViewController) {
        if let baseNavigationController = lastPresentedViewController {
            baseNavigationController.present(vc, animated: true, completion: nil)
        }
    }

}
