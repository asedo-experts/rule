import UIKit

class CompositionRoot {
    static var sharedInstance: CompositionRoot = CompositionRoot()

    var rootTabBarController: UITabBarController!

    required init() {
        
    }
    
    func pushTabBarTo(index: Int) {
        rootTabBarController.selectedIndex = index
    }
    
    // MARK: ViewControllers
    
    func resolveMainViewController() -> MainViewController {
        let vc = MainViewController.instantiateFromStoryboard("Main")
        vc.viewModel = resolveMainViewModel(view: vc)
        return vc
    }
    
    func resolveIntroductionViewController() -> IntroductionViewController {
        let vc = IntroductionViewController.instantiateFromStoryboard("Introduction")
        vc.viewModel = resolveIntroductionViewModel(view: vc)
        return vc
    }
    
    func resolveSuccessViewController() -> SuccessViewController {
        let vc = SuccessViewController.instantiateFromStoryboard("Success")
        vc.viewModel = resolveSuccessViewModel(view: vc)
        return vc
    }
    
    // MARK: ViewModels
    
    func resolveMainViewModel(view: MainView) -> MainViewModel {
        return MainViewModel(view: view)
    }
    
    func resolveIntroductionViewModel(view: IntroductionView) -> IntroductionViewModel {
        return IntroductionViewModel(view: view)
    }
    
    func resolveSuccessViewModel(view: SuccessView) -> SuccessViewModel {
        return SuccessViewModel(view: view)
    }
    
}
