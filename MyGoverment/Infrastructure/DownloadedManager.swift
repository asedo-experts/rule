import Foundation

class DownloadedManager {

    static let shared = DownloadedManager()

    func loadfile(urlfile: String, completion: @escaping (URL) -> Void, failure: @escaping (NSError?) -> Void) {
        if let url = UserDefaults.standard.string(forKey: urlfile) {
            completion(URL(fileURLWithPath: url))
            return
        }

        let fullURL = URL(string: urlfile)!

        URLSession.shared.dataTask(with: fullURL) { data, response, error in
            guard let data = data, error == nil else { return }
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
            do {
                try data.write(to: tmpURL)
            } catch {
                print(error)
                failure(error as NSError?)
            }
            DispatchQueue.main.async {
                UserDefaults.SFSDefault(setValue: tmpURL.absoluteString, forKey: urlfile)
                completion(tmpURL)
                //                self.share(url: tmpURL)
            }
        }.resume()
        //
        //        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        //
        //        let destinationUrl = documentsDirectoryURL.appendingPathComponent(fullURL.lastPathComponent)
        //        print("loadSound", destinationUrl)
        //
        //        let downloadTask = URLSession.shared.downloadTask(with: fullURL, completionHandler: { url, _, error in
        //            if error != nil {
        //                failure(error as NSError?)
        //            } else {
        //
        //                guard let url = url, error == nil else {
        //                    failure(NSError.init(domain: "", code: 500, userInfo: ["error": "Something went wrong"]))
        //                    return
        //                }
        //                do {
        //                    try FileManager.default.moveItem(at: url, to: destinationUrl)
        //
        //                    print("File moved to documents folder")
        //                    UserDefaults.SFSDefault(setValue: destinationUrl.absoluteString, forKey: urlfile)
        //                    completion(destinationUrl)
        //                } catch let error as NSError {
        //                    print("failure", error.localizedDescription)
        //                    failure(error as NSError?)
        //                }
        //            }
        //        })
        //        downloadTask.resume()
    }
}
