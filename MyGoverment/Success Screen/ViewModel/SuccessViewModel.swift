import Foundation


protocol SuccessView: class {
    func leftTime(hour: Int, minutes: Int)
}

class SuccessViewModel: ViewModel {
    
    let view: SuccessView
    
    var backgroundTimer: Timer?
    
    init(view: SuccessView) {
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateLeftTime()
        initBackgroundTimer()
    }
    
    override func willEnterForeground() {
        super.willEnterForeground()
        
        updateLeftTime()
        initBackgroundTimer()
    }
    
    private func initBackgroundTimer() {
        backgroundTimer = Timer.scheduledTimer(timeInterval: 10,
                                               target: self,
                                               selector: #selector(backgroundTimerCallBack),
                                               userInfo: nil,
                                               repeats: true)
    }
    
    private func updateLeftTime() {
        let date = Date()
        let calendar = Calendar.current
        
        let hour = 23 - calendar.component(.hour, from: date)
        let minutes = 60 - calendar.component(.minute, from: date)
        
        view.leftTime(hour: hour, minutes: minutes)
    }
    
    @objc func backgroundTimerCallBack() {
        updateLeftTime()
    }
    
}
