import UIKit

class SuccessViewController: BaseViewController<SuccessViewModel>, SuccessView {

    @IBOutlet weak var visualEffectView: UIVisualEffectView! {
        didSet {
            visualEffectView.cornerRadius(radius: 32.0)
        }
    }
    @IBOutlet weak var progressBarView: UIProgressView!
    @IBOutlet weak var progressView: UIView! {
        didSet {
            progressView.circleCornerRadius()
        }
    }
    @IBOutlet weak var sunView: UIView! {
        didSet {
            sunView.circleCornerRadius()
        }
    }
    @IBOutlet weak var openInstagramButton: UIButton! {
        didSet {
            openInstagramButton.cornerRadius(radius: 18.0)
        }
    }
    @IBOutlet weak var leftTimeLabel: UILabel!
    
    private var animationDuration: Double = 3
    private var animationStartDate: Date = Date()
    var displayLink: CADisplayLink?
    
    let startValue = 0.0
    let endValue = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        displayLink = CADisplayLink(target: self, selector: #selector(displayLinkHandle))
        displayLink?.add(to: .main, forMode: .default)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.25) {
            self.displaySuccessNotification(withText: "Hashtag #ruleonehourandhalf is copied. Share your today' goal with your friends",
                                            withTitle: "Hey, dude!",
                                            sticky: true,
                                            action: nil,
                                            actionName: nil)
            UIPasteboard.general.string = "#ruleonehourandhalf"
        }
    }
    
    @objc private func displayLinkHandle() {
        let now = Date()
        let elapsedTime = now.timeIntervalSince(animationStartDate)
        
        if elapsedTime > animationDuration {
            progressBarView.progress = 0
            sunView.backgroundColor = randomColor()
            
            displayLink?.invalidate()
            displayLink = nil
        } else {
            let percentage = elapsedTime / animationDuration
            let value = startValue + percentage * (endValue - startValue)
            progressBarView.progress = Float(value)
        }
    }
    
    private func randomColor() -> UIColor {
        return UIColor.random
    }

    @IBAction func openInstagramButtonPressed(_ sender: Any) {
        guard let url = URL(string: "instagram://app") else {
            self.displayWarningNotification(withText: "Sorry, we can't open Instagram through the app", sticky: false, action: nil, actionName: nil)
            return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            self.displayWarningNotification(withText: "Sorry, we can't open Instagram through the app", sticky: false, action: nil, actionName: nil)
        }
    }
    
    func leftTime(hour: Int, minutes: Int) {
        let stringToFormat = "\(hour) hour \(minutes) minutes"
        
        let dayTimeLeftAttributedString = NSAttributedString(string: stringToFormat,
                                                                      attributes: [NSAttributedString.Key.font: UIFont(name: "Ubuntu-Bold", size: 14.0)!,
                                                                                   NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        leftTimeLabel.attributedText = dayTimeLeftAttributedString
    }
    
    @IBAction func hastagButtonPressed(_ sender: Any) {
        self.displaySuccessNotification(withText: "Hashtag #ruleonehourandhalf is copied. Share your today' goal with your friends",
                                        withTitle: "Hey, dude!",
                                        sticky: true,
                                        action: nil,
                                        actionName: nil)
        UIPasteboard.general.string = "#ruleonehourandhalf"
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
