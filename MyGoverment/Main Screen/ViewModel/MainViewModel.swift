import Foundation
import AudioToolbox

protocol MainView {
    func timeIsUp()
    func timeElapsed(_ timeInSeconds: Double)
    func leftTime(hour: Int, minutes: Int)
    func updateMoney()
    func updatePercentage()
    func showSuccessScreen()
}

class MainViewModel: ViewModel {
    
    private enum ApplicationState {
        case inForeground, inBackground
    }
    
    let view: MainView
    
    private var applicationState: ApplicationState = .inForeground
    
    let notifications = Notifications.shared
    
    public var lastMoney: Float? {
        didSet {
            view.updatePercentage()
            _ = saveLastMoney()
        }
    }
    public var money: Float? {
        didSet {
            view.updateMoney()
            _ = saveMoney()
        }
    }
    @objc dynamic public var isWorking: Bool = false {
        didSet {
            UserDefaults.standard.set(isWorking, forKey: "kIsWorking")
        }
    }
    private var elapsedSeconds: Double = 0.0 {
        didSet {
            view.timeElapsed(elapsedSeconds)
        }
    }
    
    var timer: Timer?
    var backgroundTimer: Timer?
    
    init(view: MainView) {
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 4 hours 10 minutes left to the end of the day
        updateLeftTime()
        initBackgroundTimer()
        
        // Get Local Values
        lastMoney = getLastMoney()
        money = getMoney()
        
        // If you won't open this app
        minuseMoneyByTime()
        
        // Give me Bonus From Notification
        bonusCheck()
        
        // if you close app when your timer is firing
        elapsedSeconds = getTimerTime() ?? 0.0
        if elapsedSeconds > 0 {
            if let time = getTime() {
                let delta = Date() - time
                elapsedSeconds += delta
            }
            initTimer()
            isWorking = true
        }
    }
    
    override func didEnterBackground() {
        super.didEnterBackground()
        
        applicationState = .inBackground
        
        _ = saveLocalTime()
        _ = saveTimerTime()
        if isWorking {
            notifications.scheduleOnFinishLineNotification()
            notifications.scheduleFinishNotification()
            notifications.scheduleWorkingNotification()
        } else {
            notifications.schedulePerDayNotification()
        }
        pauseBackgroundTimer()
        pauseTimer()
    }
    
    override func willEnterForeground() {
        super.willEnterForeground()
        
        applicationState = .inForeground
        
        // 4 hours 10 minutes left to the end of the day
        updateLeftTime()
        initBackgroundTimer()
        
        // Get Local Values
        lastMoney = getLastMoney()
        money = getMoney()
        
        // If you won't open this app
        minuseMoneyByTime()
        
        // Give me Bonus From Notification
        bonusCheck()
        
        // if you close app when your timer is firing
        elapsedSeconds = getTimerTime() ?? 0.0
        if elapsedSeconds > 0 {
            if let time = getTime() {
                let delta = Date() - time
                elapsedSeconds += delta
            }
            initTimer()
            isWorking = true
        }
    }
    
    private func initBackgroundTimer() {
        backgroundTimer = Timer.scheduledTimer(timeInterval: 10,
                                               target: self,
                                               selector: #selector(backgroundTimerCallBack),
                                               userInfo: nil,
                                               repeats: true)
    }
    
    private func initTimer() {
        //5400
        if maxSeconds > 0 {
            timer = Timer.scheduledTimer(timeInterval: 1,
                                         target: self,
                                         selector: #selector(timerCallBack),
                                         userInfo: nil,
                                         repeats: true)
        }
    }
    
    public func letsGoButtonPressed() {
        if isWorking {
            stopTimer()
        } else {
            startTime()
        }
        isWorking = !isWorking
    }
    
    private func bonusCheck() {
        guard let isBonus = UserDefaults.standard.value(forKey: "kIsBonus") as? Bool,
            let _ = money,
            let _ = lastMoney else { return }
        if isBonus == true {
            self.lastMoney = money!
            self.money = money! + 1500.00
            UserDefaults.standard.set(false, forKey: "kIsBonus")
        }
    }
    
    private func minuseMoneyByTime() {
        guard let lastDate = getTime(),
            let _ = money,
            let _ = lastMoney else { return }
        let date = Date()
        
        let delta = NSInteger(date - lastDate)
        self.lastMoney = money!
        self.money = money! - Float(delta/120)
    }
    
    func startTime() {
        // start time
        stopTimer()
        elapsedSeconds = 0.0
        initTimer()
    }
    
    func pauseBackgroundTimer() {
        backgroundTimer?.invalidate()
        backgroundTimer = nil
    }
    
    func pauseTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    func stopTimer() {
        // reset time
        // stop time
        elapsedSeconds = 0.0
        timer?.invalidate()
        timer = nil
    }
    
    private func updateLeftTime() {
        let date = Date()
        let calendar = Calendar.current
        
        let hour = 23 - calendar.component(.hour, from: date)
        let minutes = 60 - calendar.component(.minute, from: date)
        
        view.leftTime(hour: hour, minutes: minutes)
    }
    
    private func getLastMoney() -> Float {
        if let lastMoney = UserDefaults.standard.value(forKey: "kLastMoney") as? Float {
            return lastMoney
        }
        return 0.0
    }
    
    private func getMoney() -> Float {
        if let money = UserDefaults.standard.value(forKey: "kMoney") as? Float {
            return money
        }
        return 0.0
    }
    
    private func getTimerTime() -> Double? {
        if let time = UserDefaults.standard.value(forKey: "kTimerTime") as? Double {
            return time
        }
        return nil
    }
    
    private func getTime() -> Date? {
        if let time = UserDefaults.standard.value(forKey: "kTime") as? Date {
            return time
        }
        return nil
    }
    
    private func saveLastMoney() -> Bool {
        guard let lastMoney = lastMoney else { return false }
        UserDefaults.standard.set(lastMoney, forKey: "kLastMoney")
        return true
    }
    
    private func saveMoney() -> Bool {
        guard let money = money else { return false }
        UserDefaults.standard.set(money, forKey: "kMoney")
        return true
    }
    
    private func saveTimerTime() -> Bool {
        UserDefaults.standard.set(elapsedSeconds, forKey: "kTimerTime")
        return true
    }
    
    private func saveLocalTime() -> Bool {
        UserDefaults.standard.set(Date(), forKey: "kTime")
        return true
    }
    
    private func congratsWithMoney() {
        guard let lastMoney = lastMoney else { return }
        self.lastMoney = money
        money = lastMoney + Float.random(in: 350.00..<1000.00)
        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate), nil)
        
        self.view.showSuccessScreen()
//        let random = Int.random(in: 0...1)
//        if random == 0 {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//                self.view.showSuccessScreen()
//            }
//        }
    }
    
    @objc func timerCallBack() {
        if applicationState == .inBackground {
            return
        }
        elapsedSeconds += 1
        if elapsedSeconds >= maxSeconds + 1 {
            view.timeIsUp()
            isWorking = false
            congratsWithMoney()
            stopTimer()
            _ = saveMoney()
            _ = saveLastMoney()
        } else {
//            view.timeElapsed(elapsedSeconds)
        }
    }
    
    @objc func backgroundTimerCallBack() {
        updateLeftTime()
    }
    
}
