import UIKit

class MainViewController: BaseViewController<MainViewModel>, MainView {

    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var triangleImageView: UIImageView!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var progressView: UIView! {
        didSet {
            progressView.circleCornerRadius()
        }
    }
    @IBOutlet weak var sunView: UIView! {
        didSet {
            sunView.circleCornerRadius()
        }
    }
    @IBOutlet weak var progressBarView: UIProgressView!
    @IBOutlet weak var letsGoButton: UIButton! {
        didSet {
            letsGoButton.cornerRadius(radius: 18.0)
        }
    }
    @IBOutlet weak var dayTimeLeftLabel: UILabel!
    @IBOutlet weak var visualEffectView: UIVisualEffectView! {
        didSet {
            visualEffectView.cornerRadius(radius: 32.0)
        }
    }
    
    private var displayLink: CADisplayLink?
//    private var moneyAnimationIsFinished: Bool = false {
//        didSet {
//            if moneyAnimationIsFinished == true {
//                pauseDisplayLink()
//            }
//        }
//    }
    private var lastMoney: Float? {
        get {
            return viewModel.lastMoney
        }
    }
    private var newMoney: Float? {
        get {
            return viewModel.money
        }
    }
    
    private var animationDuration: Double = 5
    private var animationStartDate: Date?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func bindWithObserver() {
        observer.from(viewModel, \.isWorking).to { isWorking in
            let title = isWorking ? "Stop" : "Let's Go"
            self.letsGoButton.setTitle(title, for: .normal)
            self.letsGoButton.backgroundColor = isWorking ? ColorName.red : ColorName.orange
        }
    }
    
    private func initDisplayLink() -> Bool {
        animationStartDate = Date()
//        moneyAnimationIsFinished = false
        if let _ = displayLink {
            resumeDisplayLink()
            return true
        }
        displayLink = CADisplayLink(target: self, selector: #selector(displayLinkHandle))
        displayLink?.add(to: .main, forMode: .default)
        return true
    }
    
    private func pauseDisplayLink() {
        displayLink?.isPaused = true
    }
    
    private func resumeDisplayLink() {
        displayLink?.isPaused = false
    }
    
    @IBAction func letsGoButtonPressed(_ sender: Any) {
        viewModel.letsGoButtonPressed()
    }

//    @IBAction func resetButtonPressed(_ sender: Any) {
//        UIView.animate(withDuration: 0.5) {
//            self.progressBarView.setProgress(0.0, animated: true)
//        }
//    }
    
    @objc func displayLinkHandle() {
        guard let lastMoney = lastMoney,
            let newMoney = newMoney,
        let animationStartDate = animationStartDate else { return }
        
        let now = Date()
        let elapsedTime = now.timeIntervalSince(animationStartDate)
        
        if elapsedTime > animationDuration {
            editMoney(money: newMoney)
        } else {
            let percentage = elapsedTime / animationDuration
            let value = lastMoney + Float(percentage) * (newMoney - lastMoney)
            editMoney(money: value)
        }
    }
    
    private func editMoney(money: Float) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        let priceFormatted = formatter.string(from: NSNumber(value: money)) ?? "nil"
        let numberMutableAttributedString = NSMutableAttributedString(string: priceFormatted,
                                                                      attributes: [NSAttributedString.Key.font: UIFont(name: "Ubuntu-Medium", size: 36.0)!])
        self.balanceLabel.attributedText = numberMutableAttributedString
    }
    
    private func editPercentage(percantage: Float) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.multiplier = 1
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 2
        
        let percentageFormatted = formatter.string(from: NSNumber(value: percantage)) ?? "nil"
        let numberMutableAttributedString = NSMutableAttributedString(string: "\(percentageFormatted)%",
                                                                      attributes: [NSAttributedString.Key.font: UIFont(name: "Ubuntu-Regular", size: 14.0)!])
        self.percentageLabel.textColor = percantage > 0 ? ColorName.green : ColorName.red
        self.triangleImageView.tintColor = percantage > 0 ? ColorName.green : ColorName.red
        self.triangleImageView.transform = percantage > 0 ? CGAffineTransform.init(rotationAngle: 2*CGFloat.pi) : CGAffineTransform.init(rotationAngle: CGFloat.pi)
        self.percentageLabel.attributedText = numberMutableAttributedString
    }
    
    // MARK: Main View
    
    func showSuccessScreen() {
        Coordinator.shared.showSuccessScreen()
    }
    
    func updateMoney() {
        guard let _ = lastMoney,
            let _ = newMoney else { return }
        
        _ = initDisplayLink()
        
        updatePercentage()
    }
    
    func updatePercentage() {
        guard let lastMoney = lastMoney,
            let newMoney = newMoney else { return }
        let percentage = (100-(newMoney*100.00)/lastMoney) * -1
        editPercentage(percantage: percentage)
    }
    
    func timeIsUp() {
        //        progressBarView.progress = 0.0
    }
    
    func timeElapsed(_ timeInSeconds: Double) {
        UIView.animate(withDuration: 0.25) {
            self.progressBarView.setProgress(Float(timeInSeconds/maxSeconds), animated: true)
        }
    }
    
    func leftTime(hour: Int, minutes: Int) {
        let stringToFormat = "\(hour) hour \(minutes) minutes"
        
        let dayTimeLeftAttributedString = NSAttributedString(string: stringToFormat,
                                                                      attributes: [NSAttributedString.Key.font: UIFont(name: "Ubuntu-Bold", size: 14.0)!,
                                                                                   NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        dayTimeLeftLabel.attributedText = dayTimeLeftAttributedString
    }
    
}
